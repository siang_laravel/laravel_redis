<?php

use App\Blog;
use App\Author;
use App\Comment;
use App\Subject;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //清除所有數據
        Blog::delete();
        Author::delete();
        Comment::delete();
        Subject::delete();
        Blogs_subjects::delete();


        // 填充blogs表，创建三篇文章
        $php = Blog::create(array('title' => 'PHP的未来' ));
        $java = Blog::create(array('title' => 'Java的未来' ));
        $html5 = Blog::create(array( 'title' => 'HTML5的未来' ));

        //命令行文字提示
        $this->command->info('The blogs are seeded!');

        // 填充author表
        Author::create(array(
            'name' => 'PHP的作者',
            'blog_id' => $php->id
        ));
        Author::create(array(
            'name' => 'Java的作者',
            'blog_id' => $java->id
        ));
        Author::create(array(
            'name' => 'HTML5的作者',
            'blog_id' => $html5->id
        ));
        
        // 填充comments表
        Comment::create(array(
            'content' => 'PHP多用于服务器程序编写',
            'blog_id' => $php->id
        ));
        Comment::create(array(
            'content' => 'PHP是无类型编程语言',
            'words' => 11,
            'blog_id' => $php->id
        ));

        // 填充subjects表
        $language = Subject::create(array(
            'name'=> '计算机语言',
        ));
        $program = Subject::create(array(
            'name'=> '编程语言',
        ));

        // 关联blogs与subjects
        $php->subjects()->attach($language->id);
        $php->subjects()->attach($program->id);
        $java->subjects()->attach($language->id);
        $java->subjects()->attach($program->id);
        $html5->subjects()->attach($language->id);

        
    }
}
