<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    //以下字段可以被批量赋值
    protected $fillable = array('name','blog_id');
    //新的表名，不使用默认authors表名
    //protected $table = 'authors';
    public function blog()
    {
        return $this->belongsTo(Blog::class);
    }

}
