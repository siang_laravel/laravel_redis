<?php
namespace App\Repos;

use App\Blog;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

class BlogRepo
{
    protected   $blog;
    protected   $trendingPostsKey;

    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
        $this->trendingPostsKey='popular_posts';
    }


    public function getById(int $id, array $columns = ['*'])
    {   
        $cacheKey = 'post_' . $id;
        $redis = Cache::getRedis();
        //dd($redis);
        return Cache::remember($cacheKey, 1 * 60 * 60, function () use ($id, $columns) {
            return $this->blog->select($columns)->find($id);
        });

        // $cacheKey='post_'.$id;
        // if(Redis::exists($cacheKey)){ //基于 EXISTS 指令判断对应缓存项在 Redis 中是否存在
        //     return unserialize(Redis::get($cacheKey)); //若缓存项存在，则通过 Redis 的 GET 指令获取该缓存项直接返回（如果缓存项是对象，则通过 unserialize 方法对其做反序列化操作再返回）
        // }else{
        //     //若缓存项不存在，则先通过数据库查询获取结果，然后基于 Redis 的 SETEX 指令将其存储到 Redis（如果待缓存项是对象，则通过 serialize 方法将其序列化为文本字符串，这里使用 SETEX 指令的原因是需要设置缓存过期时间），再返回数据库查询结果。
        //     $blog=$this->blog->select($columns)->find($id);
        //     if (!$blog) {
        //         return null;
        //     }
        //     Redis::setex($cacheKey,1*60*15,serialize($blog));//緩存15分鐘
        //     return  $blog;
        // }

        //return $this->blog->select($columns)->find($id);
    }

    public function getByManyId(array $ids, array $columns = ['*'], callable $callback = null)
    {
        $query = $this->blog->select($columns)->whereIn('id', $ids);
        if ($query) {
            $query = $callback($query);
        }
        return $query->get();
    }

    public function addViews(blog $blog)
    {
        $blog->increment('views');
        if ($blog->save()) {
            // 将当前文章浏览数 +1，存储到对应 Sorted Set 的 score 字段
            Redis::zincrby($this->trendingPostsKey, 1, $blog->id);
        }
        
        return $blog->views;
    }

    public function pushAddViews(blog $blog)
    {
        Redis::rpush('push-view-increment',$blog->id);
        return ++$blog->views;
    }

    // 热门文章排行榜
    public function trending($num = 10)
    {   
        $cacheKey=$this->trendingPostsKey.'_'.$num;
        //dd($cacheKey);
        return Cache::remember($cacheKey, 10 * 60, function () use ($num) {
            $postIds = Redis::zrevrange($this->trendingPostsKey, 0, $num - 1);
            //dd($postIds);
            if ($postIds) {
                $idsStr = implode(',', $postIds);
                return $this->getByManyId($postIds, ['*'], function ($query) use ($idsStr) {
                    return $query->orderByRaw('field(`id`, ' . $idsStr . ')');
                });
            }
        });
        
        // if(Redis::exists($cacheKey)){ //基于 EXISTS 指令判断对应缓存项在 Redis 中是否存在
        //     return unserialize(Redis::get($cacheKey)); //若缓存项存在，则通过 Redis 的 GET 指令获取该缓存项直接返回（如果缓存项是对象，则通过 unserialize 方法对其做反序列化操作再返回）
        // }else{

        //     $postIds = Redis::zrevrange($this->trendingPostsKey, 0, $num - 1);
        //     if (!$postIds) {
        //         return null;
        //     }
        //     $idsStr = implode(',', $postIds);
            
        //     $blogs= $this->getByManyId($postIds, ['*'], function ($query) use ($idsStr) {
        //         return $query->orderByRaw('field(`id`, ' . $idsStr . ')');
        //     });

        //     Redis::setex($cacheKey,1*60*15,serialize($blogs));//緩存15分鐘
        //     return $blogs;
        // }
    }

}