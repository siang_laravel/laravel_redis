<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = array('name');
   
    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'blogs_subjects', 'subject_id', 'blog_id');
    }

}
