<?php

namespace App\Listeners;

use App\Events\BlogViewed;
use Illuminate\Support\Facades\Redis;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class IncreaseBlogViews implements ShouldQueue
{
    use InteractsWithQueue;
    public  $queue = 'events';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(BlogViewed $event)
    {
        if ($event->blog->increment('views')) {
            Redis::zincrby('popular_posts', 1, $event->blog->id);
            return  $event->blog->id;
        }
    }
}
