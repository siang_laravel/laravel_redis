<?php

namespace App\Console\Commands;

use Redis;
use Illuminate\Console\Command;

class SubscribeMsg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //控制台指令的名称
    protected $signature = 'Sub:Msg';
    



    /**
     * The console command description.
     *
     * @var string
     */
    //控制台指令的描述
    protected $description = 'Redis Subscribe Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //return 0;
        Redis::subscribe(['redis-msg'], function($message) {
            echo  $message.PHP_EOL;
        });   

    }
}
