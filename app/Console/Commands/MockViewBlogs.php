<?php

namespace App\Console\Commands;

use App\Blog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redis;

class MockViewBlogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mock:view-blog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command view-blog';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // 1、先清空 posts 表
        Blog::truncate();
        // 2、删除对应的 Redis 键
        Redis::del('popular_posts');
        // 3、生成 100 篇测试文章
        factory(Blog::class,100)->create();

        // 4、模拟对所有文章进行 10000 次随机访问
        for ($i = 0; $i < 100; $i++) {
            $postId = mt_rand(1, 100);
            $response = Http::get('http://localhost:8000/blogs/' . $postId);
            $this->info($response->body());
        }
        return 0;
    }
}
