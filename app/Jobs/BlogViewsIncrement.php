<?php

namespace App\Jobs;

use App\Blog;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Redis;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class BlogViewsIncrement implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $blog;
   
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
       
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        
        if ($this->blog->increment('views')) {
            //dd($this->blog);
            Redis::zincrby('popular_posts', 1, $this->blog->id);
        }
    }
}
