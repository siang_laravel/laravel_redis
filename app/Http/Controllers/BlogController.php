<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Repos\BlogRepo;
use App\Events\BlogViewed;
use Illuminate\Http\Request;
use App\Jobs\BlogViewsIncrement;
use Illuminate\Support\Facades\Redis;

class BlogController extends Controller
{
    protected   $blogRepo;

    public function __construct(BlogRepo $blogRepo)
    {
        $this->blogRepo = $blogRepo;
       
    }

    // public function show(Request $request,Blog $blog){
        
    //     $blog->increment('views');
        
    //     if ($blog->save()) {
    //         // 将当前文章浏览数 +1，存储到对应 Sorted Set 的 score 字段
    //         Redis::zincrby('popular_posts', 1, $blog->id);
    //     }

    //     return 'Show Post #' . $blog->id;

    // }

     // 浏览文章
     public function show($id)
     {
        
         $blog = $this->blogRepo->getById($id);
         //$views = $this->blogRepo->addViews($blog);
         //$views = $this->blogRepo->pushAddViews($blog);
         //$this->dispatch(new BlogViewsIncrement($blog));
         event(new BlogViewed($blog));
         return "Show Post #{$blog->id}, Views: {$blog->views}";
     }


    // 获取热门文章排行榜
    public function index(Request $request)
    {
        //dd($request);
        // 获取浏览器最多的前十篇文章
        // $blogIds=Redis::zrevrange('popular_posts',0,9);
        
        // if($blogIds){
        //     $idsStr = implode(',', $blogIds);
        //     //dd($idsStr);
        //     // 查询结果排序必须和传入时的 ID 排序一致
        //     // $posts = Blog::whereIn('id', $blogIds)
        //     //     ->select(['id', 'title', 'views'])
        //     //     ->orderByRaw('field(`id`, ' . $idsStr . ')')
        //     //     ->get();  
        // }else{
        //     $blogs=null;
        // }

        $blogs = $this->blogRepo->trending(10);
        if ($blogs) {
            dd($blogs->toArray());
    
        }

    }    
}
