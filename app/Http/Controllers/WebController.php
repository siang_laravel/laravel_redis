<?php

namespace App\Http\Controllers;

use Redis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class WebController extends Controller
{
    public function index(Request $request)
    {   
        
        $this->dispatch(new QueuedTest());
        //消息的获取与处理
        $queue = app('Illuminate\Contracts\Queue\Queue');
        $queueJob = $queue->pop();
        $queueJob->fire();
        //Redis::set('string:user:name','eric');

        //echo Redis::get('string:user:name');
        return view('welcome');
    }
}
