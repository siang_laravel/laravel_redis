<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //以下字段可以被批量赋值
    protected $fillable = array('title');
   
    // 定义文章模型关系，一篇文章只能有一个作者
    public function author()
    {
        return $this->hasOne(Author::class);
    }
    
    // 一篇文章有多条评论
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    // 一篇文章可以在多个专题中，一个专题可以包含多篇文章
    public function subjects()
    {
        return $this->belongsToMany(Subject::class,'blogs_subjects','blog_id', 'subject_id');
    }

}
