<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogs_subjects extends Model
{
    protected $table    = 'blogs_subjects';
    protected $fillable = array('blog_id','subject_id');
}
